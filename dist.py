import sys
import math

def read_bed(fname):
    l1 = []
    with open(fname, "r") as fh:
        for line in fh:
            ligne = line.split("\t")
            l1.append(ligne[0:4])
    return l1

import sys

def read_bed(fname):
    l1 = []
    with open(fname, "r") as fh:
        for line in fh:
            ligne = line.split("\t")
            l1.append(ligne[0:4])
    return l1

def is_bed(fname):
    import os
    dir_path = os.path.dirname(os.path.realpath(__file__))
    os.chdir(dir_path)
    extension=os.path.splitext(fname)
    try:
        if ".bed" in extension[1]:
            #print ("le fichier est de type .bed")
            with open(fname, "r") as fh: # La première colonne c'est avec chr
                firstline=fh.readline().rstrip()
                firstline = firstline.split("\t")
                tryint1=int(firstline[1])
                tryint2=int(firstline[2])
                if firstline[0][:3] == 'chr' and len(firstline[3]) > 1:

                    return True
    except:
        return False

def write_bed(fname, distList):
    with open(fname, "w") as outputFh:
        for dist in distList:
            outputFh.write(('\t').join(dist.split(":"))+'\n')

def calcDist(listGene, listRegions, seuil):
    listChr = list()
    for gene in listGene:
        if gene[0] not in listChr:
            listChr.append(gene[0])
    for region in listRegions:
        if region[0] not in listChr:
            listChr.append(region[0])

    finalList = list()

    for chr in listChr:
        for gene in listGene:
            if gene[0] == chr:
                for region in listRegions:
                    if region[0] == chr:
                        startGene, stopGene = int(gene[1]), int(gene[2]) #1,5
                        startRegion, stopRegion = int(region[1]), int(region[2]) #3,4
                        infos = ":".join([chr, region[3].rstrip(), gene[3].rstrip()])

                        if min(startGene, stopGene) < min(startRegion, stopRegion): 
                            #gene commence avant region
                            if max(startGene, stopGene) < min(startRegion, stopRegion): 
                                #gene situé avant region
                                infos += ":" + str(abs(max(startGene, stopGene) - min(startRegion, stopRegion))) 
                            else: 
                                #gene commence avant mais region commence avant fin gene donc chauvauchement
                                infos += ":" + str(0)

                        else: 
                            #region commence avant gene
                            if max(startRegion, stopRegion) < min(startGene, stopGene): 
                                #region située avant gene
                                infos += ":" + str(abs(max(startRegion, stopRegion) - min(startGene, stopGene)))
                            else: 
                                #region commence avant mais gene commence avant fin region donc chauvauchement
                                infos += ":" + str(0)
                        
                        if int(infos.split(":")[3]) < seuil: 
                            finalList.append(infos)
    return finalList 

def display_help():
    print("\nCalcule toutes les distances entre des gènes et les régions d’intérêt d'un chromosome puis liste uniquement les couples (gène-région) dont la distance est inférieure à un seuil donné. \n Les positions des gènes et des régions sont définies dans des fichiers .bed. \n\n Usage: \n dist.py [Options] \n \n Options: \n -i [genefile.bed] [regionfile.bed] Prend en input deux fichiers .bed, en première position les localisations des gènes, en seconde position les localisations des régions. \n -o [file] Permet d'enregistrer la sortie dans le fichier indiqué. \n -s [seuil] Permet d'indiquer la distance seuil. Le seuil par défaut est 0. \n -h Affiche l'aide. \n ")


if __name__ == "__main__":
    params=[]
    for i in range (1,9):
        try:
            arg = sys.argv[i]
            params.append(arg)
        except:
            pass


    file1,file2,seuil,output = "None","None",math.inf,"None"

    for i in range(len(params)):
        if params[i] == "-h":
            display_help()
        elif params[i] == "-i":
            file1 = params[i+1]
            file2 = params[i+2]

            if file1 == "-o" or file1 == "-s" or file1 == "-h" or file1 == "None":
                raise "filename error"
            if file2 == "-o" or file2 == "-s" or file2 == "-h" or file1 == "None":
                raise "filename error"

        elif params[i] == "-s":
            seuil = params[i+1]
            if seuil == "-o" or seuil == "-i" or seuil == "-h"or file1 == "None":
                raise "erreur valeur seuil"
        elif params[i] == "-o":
            output = params[i+1]
            if seuil == "-s" or seuil == "-i" or seuil == "-h" or file1 == "None":
                raise "erreur out name"
        else:
            pass
                
    if file1 == "None" or file2 == "None":
        print("Problème de fichier")
        display_help()

    elif not is_bed(file1) or not is_bed(file2):
        print("Au moins un des fichier n'est pas un .bed")

    else:
        listGene = read_bed(file1)
        listRegion = read_bed(file2)

        if output == "None": 
            for dist in calcDist(listGene, listRegion, 50000000):
                print(dist)
        else:
            write_bed(output, calcDist(listGene, listRegion, 50000000))
